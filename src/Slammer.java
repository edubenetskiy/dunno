import java.util.*;
import java.util.stream.Collectors;

class Slammer implements Spot {

    private final HashSet<Shorty> prisoners;
    private final Vector<Doss> dosses;
    private final Door door;
    private final Ceiling ceiling;
    private Floor floor;
    private Vector<Window> windows;

    Slammer(int numberOfDosses, int numberOfWindows) {
        this.floor = new Floor();
        this.ceiling = new Ceiling();
        this.door = new Door();

        this.windows = new Vector<>();
        for (int i = 0; i < numberOfWindows; i++)
            windows.add(new Window());

        this.dosses = new Vector<>();
        for (int i = 0; i < numberOfDosses; i++)
            dosses.add(new Doss());

        this.prisoners = new HashSet<>();
    }

    void imprison(Shorty shorty) {
        getDoor().open();
        shorty.moveTo(this);
        getDoor().close();
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "каталажка";
            case GENITIVE:
                return "каталажки";
            case ALLATIVE:
            case ACCUSATIVE:
                return "каталажку";
            case INSTRUMENTAL:
                return "каталажкой";
            default:
                return "каталажке";
        }
    }

    public Door getDoor() {
        return this.door;
    }

    public List<Berth> getBerths() {
        // Во имя инкапсуляции заклинаем этот метод возвращать скопированный
        // вектор полок, чтобы он не был изменен извне.
        return dosses.stream().collect(Collectors.toList());
    }

    public List<Shorty> getOccupants() {
        // Во имя инкапсуляции заклинаем этот метод возвращать скопированный
        // список коротышек, чтобы данные не были изменены без нашего ведома.
        return prisoners.stream().collect(Collectors.toList());
    }

    /**
     * @return <code>true</code>, если в каталажке есть свободное место.
     */
    public boolean hasFreeSpace() {
        return findFreeBerth().isPresent();
    }

    public void place(Shorty shorty) {
        if (shorty == null) {
            // Нельзя добавить в комнату null'евого коротышку.
            System.out.printf("Призрак Нуль пытается проникнуть в %s. %s начинает сходить с ума.\n", this.getName(Case.ALLATIVE), this.getName());
            return;
        }

        if (this.hasFreeSpace()) {
            boolean heWasNew = prisoners.add(shorty);

            if (heWasNew) {
                System.out.printf("%s попадает в %s.\n", shorty.getName(), this.getName(Case.ACCUSATIVE));
            } else {
                // В комнате уже был этот коротышка. Сделаем вид, что всё (почти) нормально.
                System.out.printf("%s почувствовал что-то неладное, когда его выгнали из %s и снова туда поместили.\n",
                    shorty.getName(), getName(Case.GENITIVE));
            }
        } else {
            System.out.printf("К сожалению, в %s не хватило места для ещё одного коротышки.\n", this.getName(Case.LOCATIVE));
        }
    }

    // Удалить человека из комнаты
    public void displace(Shorty shorty) {
        if (shorty == null) {
            System.out.printf("Вы когда-нибудь видели, как Никто уходит из %s? Вот и я не видел.",
                getName(Case.GENITIVE));
            return;
        }

        boolean heWasHere = prisoners.remove(shorty);

        if (heWasHere) {
            System.out.printf("Вот и не стало в %s коротышки по имени %s.\n",
                this.getName(Case.LOCATIVE), shorty.getName());
        } else {
            // Коротышки не было в комнате.
            System.out.printf("Только что %s попробовал покинуть %s, где его, собственно говоря, и не было.",
                shorty.getName(Case.ACCUSATIVE), this.getName());
        }
    }

    public String toString() {
        StringBuilder result = new StringBuilder("Slammer {\n");
        getBerths().forEach(berth -> {
            result.append("    ");
            result.append(berth.toString());
            result.append("\n");
        });
        return result.append("}").toString();
    }

    public Floor getFloor() {
        return floor;
    }

    /**
     * @return Экземпляр <code>Optional</code>, содержащий найденную свободную койку,
     * или пустой <code>Optional</code>, если все койкоместа заняты.
     */
    public Optional<Berth> findFreeBerth() {
        return getBerths().stream()
            .filter(Berth::hasFreeSpace)
            .findFirst();
    }

    /**
     * @return Общее количество койкомест в помещении.
     */
    public int getCapacity() {
        return getBerths().stream()
            .map(Berth::getCapacity)
            .reduce(0, Integer::sum);
    }

    public List<Window> getWindows() {
        return windows;
    }

    public Ceiling getCeiling() {
        return ceiling;
    }

    /**
     * Потолок помещения.
     */
    public class Ceiling implements Visible {

        public String getName(Case grammaticalCase) {
            switch (grammaticalCase) {
                case INSTRUMENTAL:
                case LOCATIVE:
                    return "потолком";
                case GENITIVE:
                    return "потолка";
                default:
                    return "потолок";
            }
        }
    }

    /**
     * Пол помещения.
     */
    public class Floor implements Spot {

        private HashSet<Shorty> occupants;

        private Floor() {
            occupants = new HashSet<>();
        }

        public String getName(Case grammaticalCase) {
            switch (grammaticalCase) {
                case INSTRUMENTAL:
                    return "полом";
                case GENITIVE:
                    return "пола";
                case LOCATIVE:
                    return "полу";
                default:
                    return "пол";
            }
        }

        public void place(Shorty shorty) {
            occupants.add(shorty);
            System.out.printf("%s садится на %s.\n", shorty.getName(), getName(Case.ALLATIVE));
        }

        public void displace(Shorty shorty) {
            System.out.printf("%s встаёт с %s.\n",
                shorty.getName(),
                getName(Case.GENITIVE));
            occupants.remove(shorty);
        }

        public List<Shorty> getOccupants() {
            return occupants.stream().collect(Collectors.toList());
        }

        public boolean hasFreeSpace() {
            return true;
        }
    }

    /**
     * Окно помещения.
     */
    public class Window {
    }

    /**
     * Дверь, ведущая в каталажку.
     */
    public class Door implements Leanable, Closable {

        public String getName(Case grammaticalCase) {

            String word;
            switch (grammaticalCase) {
                case NOMINATIVE:
                case ACCUSATIVE:
                    word = "дверь";
                    break;
                case INSTRUMENTAL:
                    word = "дверью";
                    break;
                default:
                    word = "двери";
                    break;
            }

            return String.format("%s в %s",
                word,
                Slammer.this.getName(Case.ACCUSATIVE));
        }
    }
}
