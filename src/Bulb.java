import java.util.Optional;

/**
 * Лампочка.
 */
public class Bulb implements Visible {

    private Slammer.Ceiling ceiling;
    private boolean bright;

    Bulb(boolean bright) {
        this.bright = bright;
    }

    /**
     * Осветить что-либо.
     *
     * @param something То, что нужно осветить.
     */
    public void light(Visible something) {

        // Если лампочка отключена, она не может светить.
        if (!getCeiling().isPresent()) throw new DetachedBulbException();

        System.out.printf("Очевидно, %s освещается единственной %s, висящей высоко под %s; ",
            something.getName(),
            getName(Case.INSTRUMENTAL),
            getCeiling().get().getName(Case.LOCATIVE));

        System.out.printf("%s %s.\n",
            getName(),
            isBright()
                ? "яркая"
                : "тусклая и светит только себе под нос");
    }

    /**
     * @return Потолок, на котором висит лампочка.
     */
    public Optional<Slammer.Ceiling> getCeiling() {
        return Optional.ofNullable(ceiling);
    }

    /**
     * Повесить лампочку на потолок заданного помещения.
     *
     * @param slammer Помещение, в которое нужно повесить лампочку.
     */
    public void hang(Slammer slammer) {
        this.ceiling = slammer.getCeiling();
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "лампочка";
            case INSTRUMENTAL:
                return "лампочкой";
            case GENITIVE:
                return "лампочки";
            case ACCUSATIVE:
                return "лампочку";
            default:
                return "лампочке";
        }
    }

    private boolean isBright() {
        return bright;
    }

    /**
     * Исключение, возникающее при попытке засветить отключённую (не висящую на потолке) лампочку.
     */
    public class DetachedBulbException extends RuntimeException {
    }
}
