/**
 * Предмет, который можно использовать для протирания чего-нибудь.
 */
public interface Wiper extends Visible {
}
