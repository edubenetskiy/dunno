/**
 * Незнайка — крайне любознательный и авантюрный коротышка из Цветочного города.
 */
public class Dunno extends Shorty {

    public Dunno() {
        super("Незнайка", "Незнайки", Curiosity.SUPERFLUOUS);
    }

    public void recede(boolean fearfully) {
        // Незнайка не знает страха (если верить его словам).
        System.out.printf("%s %s.\n", getName(),
            fearfully ? "стратегически отступает" : "делает шаг назад");
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "Незнайка";
            case GENITIVE:
                return "Незнайки";
            case ACCUSATIVE:
                return "Незнайку";
            case INSTRUMENTAL:
                return "Незнайкой";
            default:
                return "Незнайке";
        }
    }
}
