/**
 * Объекты, реализующие этот интерфейс, имеют склоняемое название или имя.
 */
public interface Named {

    /**
     * Возвращает строку с описанием объекта в именительном падеже.
     */
    default String getName() {
        return getName(Case.NOMINATIVE);
    }

    /**
     * Возвращает описание в заданном падеже.
     */
    String getName(Case grammaticalCase);
}
