/**
 * Деревянная полка, используемая для сна.
 * Деревянная полка может вместить на себе не более двух коротышек.
 */
public class Doss extends Berth {
    Doss() {
        super(/* capacity = */ 2);
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "деревянная полка";
            case GENITIVE:
                return "деревянной полки";
            case LOCATIVE:
                return "деревянной полке";
            case INSTRUMENTAL:
                return "деревянной полкой";
            default:
                return "деревянную полку";
        }
    }

    public String toString() {
        return String.format("Doss { occupants = %s }", getOccupants());
    }
}
