/**
 * Игра.
 */
public class Game implements Named {

    private String accusativeName;

    public Game(String accusativeName) {
        this.accusativeName = accusativeName;
    }

    public String getName(Case grammaticalCase) {
        return accusativeName;
    }
}
