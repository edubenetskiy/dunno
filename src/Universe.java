import java.util.Collection;
import java.util.Random;
import java.util.Vector;
import java.util.stream.Collectors;

/**
 * Главный класс программного пакета, управляющий поведением
 * всех описываемых сущностей в данной истории.
 * <p>
 * <blockquote>
 * «Увидев новоприбывшего, несколько самых любопытных коротышек соскочили
 * со своих полок и подбежали к нему. Незнайка в испуге попятился и,
 * прижавшись спиной к двери, приготовился защищаться. Разглядев его
 * измазанную физиономию, коротышки невольно рассмеялись. Незнайка понял,
 * что бояться не надо, и его лицо тоже расплылось в улыбке».
 * <div align="right">Н. Носов</div>
 * </blockquote>
 */
public class Universe {

    /**
     * Точка входа в программу.
     *
     * @param args параметры командной строки игнорируются.
     */
    public static void main(String[] args) {

        // Здесь описывается каталажка — место действия сюжета — и её узники.
        Slammer slammer = new Slammer(
            /* numberOfDosses = */ 10,
            /* numberOfWindows = */ 0
        );
        Bulb bulb = new Bulb(false);
        bulb.hang(slammer);

        CastIronStove stove = new CastIronStove(slammer);

        Vector<Shorty> prisoners = new Vector<>();

        // Имена указаны в именительном и родительном падежах, чтобы обеспечить склонение в текстах.
        prisoners.add(new Shorty("Стрига", "Стриги", Shorty.Curiosity.HIGH));
        prisoners.add(new Shorty("Вихор", "Вихра", Shorty.Curiosity.HIGH));
        prisoners.add(new Shorty("Козлик", "Козлика", Shorty.Curiosity.MEDIUM));
        prisoners.add(new Shorty("Мига", "Миги", Shorty.Curiosity.HIGH));
        prisoners.add(new Shorty("Пестренький", "Пестренького", Shorty.Curiosity.LOW));

        Random random = new Random();

        // Коротышки попадают в каталажку и начинают заниматься разными делами.
        prisoners.forEach(shorty -> {
            slammer.imprison(shorty);
            switch (random.nextInt(3)) {
                case 0:
                    shorty.moveTo(slammer.getFloor());
                    break;
                case 1:
                    shorty.moveTo(slammer.findFreeBerth().get());
                    break;
                case 2:
                    shorty.moveTo(stove);
                    break;
            }
            System.out.println();
        });

        // В центре каталажки — печь.
        System.out.printf("Посреди %s стоит %s.\n",
            slammer.getName(Case.GENITIVE),
            stove.getName(Case.NOMINATIVE));

        // От печи тянутся жестяные трубы.
        System.out.printf("От %s через всё помещение тянутся длинные %s.\n",
            stove.getName(Case.GENITIVE),
            stove.getPipes().getName(Case.NOMINATIVE));

        // Некоторые коротышки сидят у печки.
        if (!stove.getOccupants().isEmpty()) {
            String messageFormat = (stove.getOccupants().size() == 1)
                ? "У %s сидит %s.\n"
                : "Вокруг %s сидят %s.\n";

            System.out.printf(messageFormat,
                stove.getName(Case.GENITIVE),
                enumerateShorties(stove.getOccupants()));
        }

        System.out.println();

        // Кто-то запекает картофель.
        for (Shorty shorty : stove.getOccupants()) {
            if (random.nextBoolean()) {
                shorty.bakePotato(new Potato(), stove);
                System.out.println();
            }
        }

        // Кто-то сидит, свесив ноги с полок, и штопает одежонку.
        slammer.getBerths().stream()
            .flatMap(b -> b.getOccupants().stream())
            .forEach(shorty -> {
                System.out.printf("%s сидит на полке.\n", shorty.getName());

                if (random.nextBoolean()) {
                    shorty.darn(shorty.getDud());
                } else {
                    shorty.tell(new Story());
                }

                System.out.println();
            });

        // Кто-то сидит на полу, играет в расшибалочку или рассказывает истории из жизни.
        Game rasschibalotschka = new Game("расшибалочку");
        slammer.getFloor().getOccupants().forEach((Shorty shorty) -> {
            System.out.printf("%s сидит на полу.\n", shorty.getName());

            if (random.nextBoolean()) {
                shorty.play(rasschibalotschka);
            } else {
                shorty.tell(new Story());
            }

            System.out.println();
        });

        // Окон в каталажке нет.
        System.out.printf("В помещении %s окон.\n", slammer.getWindows().size());

        try {
            // Лампочка тусклая и светит себе под нос.
            bulb.light(slammer);
        } catch (Bulb.DetachedBulbException exception) {
            System.out.printf("%s не освещена, поскольку %s не подключена.",
                slammer.getName(),
                bulb.getName());
        }

        System.out.println();

        // Незнайка прилетает на Луну, где-то пачкается...
        Dunno dunno = new Dunno();

        dunno.getHands().pollute(new Pollutant() {
            public String getName(Case grammaticalCase) {
                switch (grammaticalCase) {
                    case NOMINATIVE:
                        return "чёрная краска";
                    case GENITIVE:
                        return "чёрной краски";
                    case INSTRUMENTAL:
                        return "чёрной краской";
                    case LOCATIVE:
                        return "чёрной краске";
                    default:
                        return "чёрную краску";
                }
            }
        });

        // ...не оплачивает обед в ресторане и попадает в каталажку.
        slammer.imprison(dunno);
        System.out.println();

        // Попав в каталажку, Незнайка протирает руками глаза.
        dunno.wipe(dunno.getHands(), dunno.getFace().getEyes());
        System.out.println();

        // Выбираем наиболее любопытных коротышек.
        Collection<Shorty> subjects = prisoners.stream()
            .filter(Shorty::isCurious)
            .collect(Collectors.toList());

        System.out.printf(
            "Наиболее любопытные коротышки в %s — это %s.\n",
            slammer.getName(Case.LOCATIVE), enumerateShorties(subjects));
        System.out.println();

        // Любопытные коротышки начинают интересоваться новоприбывшим Незнайкой.
        subjects.forEach(shorty -> {
            shorty.lookAt(dunno);
            shorty.leave();
            shorty.runTo(dunno);
            System.out.println();
        });

        // Незнайке страшно, но он не показывает вида.
        // Во всяком случае, потом он так всем и расскажет.
        dunno.recede(true);
        dunno.leanOn(slammer.getDoor());
        dunno.prepareTo("защищаться");
        System.out.println();

        // Коротышки смотрят на грязное лицо Незнайки и смеются.
        subjects.forEach(shorty -> {
            shorty.lookAt(dunno.getFace());
            System.out.println();
        });

        // Незнайка успокаивается.
        dunno.understand("бояться не надо");
        dunno.getFace().smile();
        dunno.leanOff();
    }

    /**
     * Перечислить имена коротышек через запятую.
     *
     * @param shorties список коротышек
     * @return строка, содержащая имена коротышек, разделённые запятыми.
     */
    private static String enumerateShorties(Collection<Shorty> shorties) {
        return String.join(", ", shorties.stream()
            .map(Visible::getName)
            .collect(Collectors.toList()));
    }
}
