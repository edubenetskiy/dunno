import java.util.Objects;
import java.util.Optional;

/**
 * Коротышки, будь то из Цветочного города, Солнечного города или даже с Луны.
 *
 * @author Николай Николаевич Носов
 */
class Shorty implements Visible {

    private final String name;
    private final String genitiveName;
    private final Curiosity curiosity;

    private final Face face;
    private final Hands hands;

    private Dud dud;
    private Spot spot;
    private Leanable support;

    Shorty(String name, String genitiveName, Curiosity curiosity) {
        this.name = name;
        this.genitiveName = genitiveName;
        this.curiosity = curiosity;

        this.face = new Face();
        this.hands = new Hands();

        this.dud = new Dud();
        this.spot = null;
        this.support = null;
    }

    /**
     * Испечь картошку в печи.
     */
    void bakePotato(Potato potato, CastIronStove stove) {

        open(stove.getFlap());

        System.out.printf("%s кидает %s в горячую %s.\n",
            getName(),
            potato.getName(Case.ACCUSATIVE),
            stove.getCinder().getName(Case.ALLATIVE));

        stove.getCinder().throwPotato(potato);

        System.out.printf("%s вытаскивает из %s %s.\n",
            getName(),
            stove.getCinder().getName(Case.GENITIVE),
            potato.getName(Case.ACCUSATIVE));

        Potato bakedPotato = stove.getCinder().takePotato();

        if (bakedPotato.isHot()) {
            System.out.printf("%s неожиданно для себя обнаруживает, что %s горячая.\n",
                getName(),
                bakedPotato.getName());
            cool(bakedPotato);
        } else {
            System.out.printf("Оказывается, что %s имеет пригодную для употребления температуру.",
                bakedPotato.getName());
        }
    }

    private void cool(Potato potato) {
        System.out.printf("%s пытается остудить %s.\n",
            getName(),
            potato.getName(Case.ACCUSATIVE));
        blow(potato);
        hands.juggle(potato);
    }

    private void blow(Potato potato) {
        System.out.printf("%s усиленно дует на %s.\n",
            getName(),
            potato.getName(Case.ACCUSATIVE));
    }

    private void open(Closable something) {
        System.out.printf("%s открывает %s.\n",
            getName(),
            something.getName(Case.ACCUSATIVE));
        something.open();
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case GENITIVE:
                return genitiveName;
            default:
                return name;
        }
    }

    /**
     * Определяет, является ли коротышка любопытным.
     *
     * @return <code>true</code>, если уровень любопытства коротышки равен
     * или превышает <code>Shorty.Curiosity.HIGH</code>.
     * @see Curiosity
     */
    public boolean isCurious() {
        return curiosity.compareTo(Curiosity.HIGH) >= 0;
    }

    /**
     * Разместиться в указанном месте.
     *
     * @param spot место назначения
     */
    public void moveTo(Spot spot) {
        if (spot == null) {
            // Перемещение коротышки «в никуда» представляется абсурдным действом.
            System.out.printf("%s просят отправиться к чёртовой бабушке. %s вежливо отказывается.\n", getName(Case.ACCUSATIVE), getName());
        } else {
            try {
                spot.place(this);
            } catch (Spot.NoFreeSpaceFoundException e) {
                System.out.printf("%s не может разместиться в указанном месте, потому что там уже занято.\n",
                    getName(),
                    getName(Case.ALLATIVE));
            }
            this.spot = spot;
        }
    }

    /**
     * Покинуть место.
     */
    public void leave() {
        if (getSpot().isPresent()) {
            getSpot().get().displace(this);
        } else {
            System.out.printf("%s пытается покинуть своё место, хотя и не имеет его.\n", this.getName());
        }
    }

    /**
     * Подбежать к кому-нибудь.
     *
     * @param shorty тот, к кому нужно подбежать.
     */
    public void runTo(Shorty shorty) {
        if (this.equals(shorty)) {
            System.out.printf("%s замечает в своём сознании странное желание подбежать к самому себе. Жуть!\n", getName());
        } else {
            System.out.printf("%s подбегает к %s.\n", this.getName(), shorty.getName(Case.ALLATIVE));
        }
    }

    /**
     * Возвращает место, где располагается коротышка.
     *
     * @return место, которое занимает коротышка.
     */
    public Optional<Spot> getSpot() {
        return Optional.ofNullable(this.spot);
    }

    /**
     * Попятиться.
     *
     * @param fearfully пятиться в страхе — <code>true</code>, без страха — <code>false</code>.
     */
    public void recede(boolean fearfully) {
        System.out.printf("%s %s.\n", getName(), fearfully ? "в страхе пятится" : "пятится");
    }

    /**
     * Протереть что-то чем-то.
     *
     * @param wiper  чем протирать.
     * @param object что протирать.
     */
    public void wipe(Wiper wiper, Visible object) {
        System.out.printf("%s протирает %s %s.\n",
            getName(),
            wiper.getName(Case.INSTRUMENTAL),
            object.getName(Case.ACCUSATIVE));

        Optional<Pollutant> pollutant = wiper.getPollutant();

        if (pollutant.isPresent()) {
            System.out.printf("Толку из этого выходит мало.\n");
            object.pollute(pollutant.get());
        }
    }

    /**
     * Облокотиться на что-нибудь или прижаться к чему-нибудь.
     *
     * @param leanable то, к чему следует прижаться.
     */
    public void leanOn(Leanable leanable) {
        this.support = leanable;
        System.out.printf("%s прижимается к %s.\n", getName(), leanable.getName(Case.ALLATIVE));
    }

    /**
     * Перестать опираться на то, к чему сейчас прижимается коротышка.
     */
    public void leanOff() {
        if (support != null) {
            System.out.printf("%s отходит от %s.\n", getName(), support.getName(Case.GENITIVE));
            support = null;
        }
    }

    /**
     * Взглянуть на что-нибудь.
     *
     * @param something то, на что требуется посмотреть.
     */
    public void lookAt(Visible something) {
        System.out.printf(
            "%s смотрит на %s.\n",
            this.getName(),
            something.getName(Case.ACCUSATIVE));

        if (something.isFunny()) {
            laugh(false);
        }
    }

    /**
     * Подготовиться к какому-либо действию.
     *
     * @param action действие, записанное в форме инфинитива.
     */
    public void prepareTo(String action) {
        System.out.printf("%s готовится %s.\n", getName(), action.trim());
    }

    /**
     * @return Лицо данного коротышки.
     */
    public Face getFace() {
        return face;
    }

    /**
     * @return Степень любопытства коротышки.
     */
    public Curiosity getCuriosity() {
        return curiosity;
    }

    /**
     * Рассмеяться.
     *
     * @param wittingly Смеяться невольно — <code>false</code>, вольно — <code>true</code>.
     */
    private void laugh(boolean wittingly) {
        getFace().smile();
        System.out.printf("%s %s.\n", getName(), wittingly ? "смеётся" : "невольно смеётся");
    }

    public void understand(String idea) {
        System.out.printf("%s понимает: %s.\n", getName(), idea);
    }

    public void darn(Dud dud) {
        System.out.printf("%s с иглой штопает %s.\n",
            getName(),
            dud.getName(Case.ACCUSATIVE));
    }

    public boolean equals(Shorty anotherShorty) {
        return this.getName().equals(anotherShorty.getName());
    }

    public int hashCode() {
        return Objects.hash(getName(), isCurious());
    }

    public String toString() {
        return String.format("Shorty { name = \"%s\", curiosity = %s }",
            getName(),
            getCuriosity());
    }

    public Dud getDud() {
        return dud;
    }

    public void tell(Story story) {
        System.out.printf("%s рассказывает желающим послушать какую-то %s.\n",
            getName(),
            story.getName(Case.ACCUSATIVE));
    }

    public void play(Game game) {
        System.out.printf("%s играет с приятелями в %s.\n",
            getName(),
            game.getName(Case.ACCUSATIVE));
    }

    public Hands getHands() {
        return hands;
    }

    /**
     * Степень любопытства и любознательности.
     */
    enum Curiosity {

        /**
         * Коротышка практически ко всему безразличен.
         */
        LOW,

        /**
         * Коротышка любопытен так же, как большинство людей.
         */
        MEDIUM,

        /**
         * Коротышка проявляет большое любопытство ко многим предметам.
         */
        HIGH,

        /**
         * Коротышка сверх меры любопытен и суёт свой нос повсюду.
         */
        SUPERFLUOUS,
    }

    /**
     * Лицо коротышки.
     */
    public class Face implements Visible {

        private Eyes eyes;
        private Pollutant pollutant;

        private Face() {
            this.eyes = new Eyes();
            this.pollutant = null;
        }

        public boolean isFunny() {
            // Если лицо грязное, оно вызывает смех.
            return getPollutant().isPresent();
        }

        /**
         * Отобразить на лице улыбку.
         */
        public void smile() {
            System.out.printf("На %s появляется улыбка.\n", getName(Case.LOCATIVE));
        }

        public String getName(Case grammaticalCase) {
            String word;
            switch (grammaticalCase) {
                case GENITIVE:
                    word = "лица";
                    break;
                case ALLATIVE:
                    word = "лицу";
                    break;
                case LOCATIVE:
                    word = "лице";
                    break;
                case INSTRUMENTAL:
                    word = "лицом";
                    break;
                default:
                    word = "лицо";
                    break;
            }
            return String.format("%s %s", word, Shorty.this.getName(Case.GENITIVE));
        }

        public String toString() {
            return String.format("Shorty.Face { owner = %s, pollutant = %s }", Shorty.this, getPollutant());
        }

        public Eyes getEyes() {
            return eyes;
        }

        public Optional<Pollutant> getPollutant() {
            return Optional.ofNullable(pollutant);
        }

        public void pollute(Pollutant pollutant) {
            if (pollutant == null) return;

            this.pollutant = pollutant;
            System.out.printf("%s размазывает %s по своему лицу.\n",
                Shorty.this.getName(),
                pollutant.getName(Case.ACCUSATIVE),
                Face.this.getName(Case.ALLATIVE));
        }

        /**
         * Глаза.
         */
        public class Eyes implements Visible {

            public String getName(Case grammaticalCase) {
                switch (grammaticalCase) {
                    case GENITIVE:
                        return "глаз";
                    case INSTRUMENTAL:
                        return "глазами";
                    case LOCATIVE:
                        return "глазах";
                    case ALLATIVE:
                        return "глазам";
                    default:
                        return "глаза";
                }
            }

            public void pollute(Pollutant pollutant) {
                Face.this.pollute(pollutant);
            }
        }
    }

    /**
     * Одежонка.
     */
    public static class Dud implements Wiper {

        public String getName(Case grammaticalCase) {
            switch (grammaticalCase) {
                case NOMINATIVE:
                    return "одежонка";
                case GENITIVE:
                    return "одежонки";
                case INSTRUMENTAL:
                    return "одежонкой";
                case LOCATIVE:
                case ALLATIVE:
                    return "одежонке";
                default:
                    return "одежонку";
            }
        }
    }

    /**
     * Руки.
     */
    public class Hands implements Wiper {

        Pollutant pollutant;

        private Hands() {
            pollutant = null;
        }

        public String getName(Case grammaticalCase) {
            switch (grammaticalCase) {
                case GENITIVE:
                    return "рук";
                case INSTRUMENTAL:
                    return "руками";
                case ALLATIVE:
                    return "рукам";
                case LOCATIVE:
                    return "руках";
                default:
                    return "руки";
            }
        }

        public Optional<Pollutant> getPollutant() {
            return Optional.ofNullable(pollutant);
        }

        public void juggle(Potato potato) {
            System.out.printf("%s перебрасывает %s в %s.\n",
                Shorty.this.getName(),
                potato.getName(Case.ACCUSATIVE),
                getName(Case.LOCATIVE));
            potato.cool();
        }

        public void pollute(Pollutant pollutant) {
            if (pollutant == null) return;

            this.pollutant = pollutant;
            System.out.printf("%s пачкает %s %s.\n",
                Shorty.this.getName(),
                Hands.this.getName(Case.ACCUSATIVE),
                pollutant.getName(Case.INSTRUMENTAL));
        }
    }
}
