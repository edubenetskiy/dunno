import java.util.Optional;

/**
 * Нечто, на что можно взглянуть и что можно загрязнить.
 */
public interface Visible extends Named {

    /**
     * Определяет, вызывает ли смех у коротышек данный предмет.
     *
     * @return <code>true</code>, если объект смешной, иначе <code>false</code>.
     */
    default boolean isFunny() {
        // По умолчанию все объекты считаются несмешными.
        return false;
    }

    /**
     * Загрязняет предмет.
     */
    default void pollute(Pollutant pollutant) {
        // По умолчанию все предметы незагрязняемы.
        System.out.printf("%s невозможно загрязнить.", getName(Case.ACCUSATIVE));
    }

    /**
     * Определяет, чем загрязнён предмет.
     *
     * @return Загрязнитель.
     */
    default Optional<Pollutant> getPollutant() {
        return Optional.empty();
    }
}
