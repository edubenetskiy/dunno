/**
 * Предмет, который может загрязнить что-либо.
 */
public interface Pollutant extends Visible {
}
