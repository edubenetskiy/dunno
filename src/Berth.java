import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Койка — место для лежания.
 */
public abstract class Berth implements Spot {

    private final HashSet<Shorty> occupants;
    private final int capacity;

    Berth(int capacity) {
        this.capacity = capacity;
        this.occupants = new HashSet<>();
    }

    /**
     * Количество коротышек, которые могут поместиться на полке.
     *
     * @return количество спальных мест на койке.
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @return есть ли свободное место на койке.
     */
    public boolean hasFreeSpace() {
        return getOccupants().size() < getCapacity();
    }

    /**
     * @return список лежащих на койке.
     */
    public List<Shorty> getOccupants() {
        return occupants.stream().collect(Collectors.toList());
    }

    /**
     * Добавить коротышку на койку.
     */
    public void place(Shorty shorty) throws NoFreeSpaceFoundException {
        if (shorty == null) {
            System.out.printf("Барабашка безуспешно пробует разместиться на %s.", getName(Case.LOCATIVE));
            return;
        }

        if (this.hasFreeSpace()) {
            System.out.printf("%s запрыгивает на %s.\n",
                shorty.getName(),
                getName(Case.ALLATIVE));
            occupants.add(shorty);
        } else throw new NoFreeSpaceFoundException();
    }

    /**
     * Убрать коротышку с койки.
     */
    public void displace(Shorty shorty) {
        System.out.printf("%s спрыгивает с %s.\n",
            shorty.getName(),
            getName(Case.GENITIVE));
        occupants.remove(shorty);
    }
}
