/**
 * Нечто, что можно открывать и закрывать.
 */
public interface Closable extends Visible {

    /**
     * Открыть.
     */
    default void open() {
        System.out.printf("Открывается %s.\n", getName());
    }

    /**
     * Закрыть
     */
    default void close() {
        System.out.printf("Закрывается %s.\n", getName());
    }

}
