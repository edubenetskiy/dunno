import java.util.List;

public interface Spot extends Visible {

    /**
     * Разместить коротышку.
     *
     * @param shorty коротышка, которого нужно разместить.
     * @return <code>true</code>, если коротышка не <code>null</code>, ещё
     * не был размещён и ему хватило места. Во всех остальных
     * случаях метод возвращает <code>false</code>.
     */
    void place(Shorty shorty) throws NoFreeSpaceFoundException;

    /**
     * Удалить коротышку.
     *
     * @param shorty коротышка, которого нужно вытеснить с места.
     * @return <code>true</code>, если указанный коротышка был размещён ранее,
     * иначе <code>false</code>.
     */
    void displace(Shorty shorty);

    /**
     * Перечислить коротышек, занимающих данное место.
     *
     * @return Список размещённых здесь коротышек.
     */
    List<Shorty> getOccupants();

    /**
     * Определяет, есть ли свободное место.
     *
     * @return <code>true</code>, если есть свободное место,
     * иначе <code>false</code>.
     */
    boolean hasFreeSpace();

    static class NoFreeSpaceFoundException extends Exception {
    }
}
