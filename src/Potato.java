/**
 * Картофелина.
 */
public class Potato implements Visible {

    private boolean baked;
    private Temperature temperature;

    Potato() {
        this.baked = false;
        this.temperature = Temperature.ROOM;
    }

    /**
     * @return <code>true</code>, если картофелина испечена.
     */
    public boolean isBaked() {
        return baked;
    }

    public void bake() {
        if (isBaked()) {
            System.out.printf("К сожалению, %s перепеклась и обратилась в угольки.\n",
                getName());
            return;
        }

        System.out.printf("Кажется, картошка испеклась.\n", getName());
        this.baked = true;
        this.temperature = Temperature.HOT;
    }

    public void cool() {
        if (isHot()) {
            this.temperature = Temperature.WARM;
            System.out.printf("Наконец %s остывает.\n", getName());
        } else {
            // Добавим немного драматизма.
            this.temperature = Temperature.ROOM;
            System.out.printf("И без того холодная %s нарушает второе начало термодинамики и достигает температуры абсолютного нуля. Переохлаждение.\n");
        }
    }

    /**
     * @return <code>true</code>, если картофелина горячая.
     */
    public boolean isHot() {
        return temperature.equals(Temperature.HOT);
    }

    public String getName(Case grammaticalCase) {
        if (isBaked())
            switch (grammaticalCase) {
                case NOMINATIVE:
                    return "печёная картошка";
                case GENITIVE:
                    return "печёной картошки";
                case INSTRUMENTAL:
                    return "печёной картошкой";
                case LOCATIVE:
                    return "печёной картошке";
                default:
                    return "печёную картошку";
            }
        else
            switch (grammaticalCase) {
                case NOMINATIVE:
                    return "сырая картошка";
                case GENITIVE:
                    return "сырой картошки";
                case INSTRUMENTAL:
                    return "сырой картошкой";
                case LOCATIVE:
                    return "сырой картошке";
                default:
                    return "сырую картошку";
            }
    }

}
