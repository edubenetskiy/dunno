import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Чугунная печь.
 */
public class CastIronStove implements Spot, Leanable {

    private Slammer slammer;
    private TinTube pipes;
    private HashSet<Shorty> occupants;
    private Cinder cinder;
    private Flap flap;

    public CastIronStove(Slammer slammer) {
        this.slammer = slammer;
        this.pipes = new TinTube();
        this.occupants = new HashSet<>();
        this.cinder = new Cinder();
        this.flap = new Flap();
    }

    /**
     * @return Помещение, в котором расположена печь.
     */
    public Slammer getSlammer() {
        return slammer;
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "чугунная печь";
            case GENITIVE:
            case LOCATIVE:
            case ALLATIVE:
                return "чугунной печи";
            case ACCUSATIVE:
                return "чугунную печь";
            default:
                return "чугунной печью";
        }
    }

    public TinTube getPipes() {
        return pipes;
    }

    public void place(Shorty shorty) {
        occupants.add(shorty);
        System.out.printf("%s садится к %s.\n",
            shorty.getName(),
            getName(Case.ALLATIVE));
    }

    public void displace(Shorty shorty) {
        System.out.printf("%s отходит от %s.\n",
            shorty.getName(),
            getName(Case.GENITIVE));
        occupants.remove(shorty);
    }

    public List<Shorty> getOccupants() {
        return occupants.stream().collect(Collectors.toList());
    }

    public boolean hasFreeSpace() {
        return true;
    }

    public Cinder getCinder() {
        return cinder;
    }

    public Flap getFlap() {
        return flap;
    }

    /**
     * Печная дверца.
     */
    public class Flap implements Closable {
        public String getName(Case grammaticalCase) {
            switch (grammaticalCase) {
                case NOMINATIVE:
                    return "чугунная дверца";
                case ACCUSATIVE:
                    return "чугунную дверцу";
                case ALLATIVE:
                    return "чугунной дверце";
                case INSTRUMENTAL:
                    return "чугунной дверцей";
                default:
                    return "чугунной дверцы";
            }

        }
    }

    /**
     * Жестяная труба.
     */
    public class TinTube implements Leanable {
        public String getName(Case grammaticalCase) {
            switch (grammaticalCase) {
                case NOMINATIVE:
                    return "жестяные трубы";
                case ACCUSATIVE:
                case GENITIVE:
                    return "жестяных труб";
                case LOCATIVE:
                    return "жестяных трубах";
                case ALLATIVE:
                    return "жестяным трубам";
                default:
                    return "жестяными трубами";
            }
        }
    }
}
