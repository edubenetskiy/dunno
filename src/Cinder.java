import java.util.Stack;

/**
 * Зола.
 */
public class Cinder implements Visible {
    private Stack<Potato> potatoes;

    public Cinder() {
        potatoes = new Stack<>();
    }

    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "зола";
            case GENITIVE:
                return "золы";
            case INSTRUMENTAL:
                return "золой";
            case LOCATIVE:
                return "золе";
            default:
                return "золу";
        }
    }

    public void throwPotato(Potato potato) {
        potatoes.push(potato);
        potato.bake();
    }

    public Potato takePotato() {
        return potatoes.pop();
    }
}
