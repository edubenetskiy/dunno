/**
 * История.
 */
public class Story implements Named {
    public String getName(Case grammaticalCase) {
        switch (grammaticalCase) {
            case NOMINATIVE:
                return "история из жизни";
            case INSTRUMENTAL:
                return "историей из жизни";
            case ACCUSATIVE:
                return "историю из жизни";
            default:
                return "истории из жизни";
        }
    }
}
