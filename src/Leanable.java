/**
 * Предмет, на который можно опереться или облокотиться.
 */
public interface Leanable extends Visible {
}
